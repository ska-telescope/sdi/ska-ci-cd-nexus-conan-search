""" Nexus Route for managing Nexus Conan Search """
import logging
import os
import re
import time
from typing import AsyncGenerator, Optional

from fastapi import APIRouter, Query, Request
from ska_cicd_artefact_validations.validation.controllers.component_manager import (  # NOQA: E501
    ComponentManager,
)
from ska_cicd_artefact_validations.validation.models.component import Component

# ST-950: Makes more sense to have this in the controllers package
component_manager = ComponentManager()

# Repository configured with fallback
REPOSITORY = os.getenv("NEXUS_REPOSITORY", "conan-internal")

# setup logging
logger = logging.getLogger(__name__)
LOG_LEVEL = os.getenv("LOG_LEVEL", "DEBUG").upper()
logger.setLevel(LOG_LEVEL)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
formatter.converter = time.gmtime
console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)
handlers = [console_handler]
logging.basicConfig(handlers=handlers)

# start your routers!
router = APIRouter()


@router.post("/testAuth")
async def test(request: Request):
    """Just a test for auth"""
    logger.info(request)
    return


# This is the same as "/" but avoids the 307 Temporary Redirect
@router.get("")
async def ping_responder(request: Request):
    """test ping responder"""

    return "OK"


async def _get_components(repository: str) -> AsyncGenerator[Component, None]:
    """Get Component just handles the generator for component async"""
    logger.debug("Getting all %s repository components", repository)
    return component_manager.get_repository_components(repository)


# https://fastapi.tiangolo.com/tutorial/query-params-str-validations/#add-regular-expressions
@router.get("/search")
async def search_responder(
    request: Request,
    q: Optional[str] = Query(
        None, min_length=3, max_length=100, regex=r"^[a-zA-Z0-9_\-\.\*]*$"
    ),
):
    """Triggered when a conan client attempts to
    do a package search in the nexus repository"""

    # default to wildcard search if not defined
    if not q:
        q = "*"

    logger.info("Conan query: %s", q)

    results = []
    checker = re.compile(f".*{q}.*")
    async for component in await _get_components(REPOSITORY):
        logger.debug(
            "Processing %s artefact: %s/%s/%s",
            REPOSITORY,
            component.group,
            component.name,
            component.version,
        )
        if checker.search(component.name):
            vers, channel = component.version.rsplit("-")
            if not vers:
                vers = component.version
            if channel:
                channel = "/" + channel
            results.append(
                component.name + "/" + vers + "@" + component.group + channel
            )

    # conan get -r ska ds-resampler-delay-tracker/0.0.1@nrc/stable
    # is: nrc/ds-resampler-delay-tracker/0.0.1-stable
    # where group: nrc, name: ds-resampler-delay-tracker version: 0.0.1,
    #  channel: stable
    logger.debug("Results: %s", results)

    return {"results": results}
