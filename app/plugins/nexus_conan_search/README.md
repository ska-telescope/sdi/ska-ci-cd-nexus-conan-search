# SKA CI/CD Automation Services Nexus Conan Search

This plugin implements the basic Conan Search API, and is meant to be integrated with the Community Edition Nexus Repository Manager3 Conan package implementation.

This is to cover a deficiency where NX3 hosted repository does not support the package search API for Conan.

The basis for this implementation is the [GitLab API described here](https://docs.gitlab.com/ee/api/packages/conan.html#search) .  The Ping API has also been implemented (even though it exists in NX3) so that this package can be tested independently using the Conan command line client.

The implementation approach is using the SKAO System Teams FastAPI pattern based on the example here in the [ska-cicd-automation repository](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation), and makes use of the Nexus API encapsulation for querying repository artefacts defined in the [ska-cicd-artefact-validations repository](https://gitlab.com/ska-telescope/sdi/ska-cicd-artefact-validations).

This shim is deployed as part of the Central Artefact Repository Nexus deployment integrated with [HAProxy](https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-nexus/-/blob/master/playbooks/roles/haproxy/tasks/main.yaml), where the Proxy identifies the Conan Search API calls and routes these queries to the shim.
