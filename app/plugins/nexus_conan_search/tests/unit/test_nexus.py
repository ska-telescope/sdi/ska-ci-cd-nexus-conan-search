# from inspect import signature
import logging

import pytest
from fastapi.testclient import TestClient
from pytest_bdd import given, parsers, scenarios, then

from app.main import app
from app.plugins.nexus_conan_search.routers.nexus_conan_search import router

# Used to Print
logger = logging.getLogger(__name__)
# Creating a Testclient with main app
client = TestClient(app)
# Creating a Testclient with router members
client_router = TestClient(router)


@given(parsers.parse("Main route"))
def check_server():

    response = client.get("/")
    pytest.confirm = response.status_code


# Given Ping http request to /repository/conan-internal/v1/ping
@given(parsers.parse("Ping http request to {target}"))
def basic_get(
    target: str,
):

    pytest.target = target

    response = client.get(pytest.target)
    pytest.confirm = response.status_code


# Given an http request to /repository/conan-internal/v1/conans/search
@given(parsers.parse("an http request to {target}"))
def basic_search(
    target: str,
):

    pytest.target = target


# And a query of ska-example-conan-package


@given(parsers.parse("a query of {query}"))
def send_search(query: str):
    import json

    pytest.query = query

    response = client.request("GET", pytest.target, {"q": query})
    pytest.confirm = response.status_code
    pytest.body = response.content

    logger.debug("query response: %s", pytest.body)
    message_json = json.loads(pytest.body)
    logger.debug("response parsed: %s", repr(message_json))
    pytest.results = message_json["results"]
    logger.debug("response results: %s", repr(pytest.results))


@then(parsers.parse("Return code {message}"))
def check_message(message):
    assert int(message) == pytest.confirm


@then(parsers.parse("one result"))
def check_one_result():
    assert len(pytest.results) == 1


@then(parsers.parse("the result is {expected}"))
def check_one_result_expected(expected: str):
    assert expected == pytest.results[0]


scenarios("./nexus.feature")
